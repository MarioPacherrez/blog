<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Category;
use Laracasts\Flash\Flash;

class CategoriesController extends Controller
{
    public function create()
    {
    	return view('admin.categories.create');
    }

    public function store(CategoryRequest $request)
    {
		$category = new Category($request->all());
		$category->save();

		flash('La categoría '.$category->name.' ha sido creada con exito!', 'success')->important();
		return redirect()->route('categories.index');
	}

	public function index()
	{
		$categories = Category::orderBy('id','DESC')->paginate(5);
		return view('admin.categories.index')->with('categories', $categories);
	}

	public function destroy($id)
	{
		$category = Category::find($id);
		$category->delete();

		flash('La Categoría '.$category->name.' ha sido borrado con exito!', 'warning')->important();
		return redirect()->route('categories.index');
	}
	public function edit($id)
	{
		$category = Category::find($id);
		return view('admin.categories.edit')->with('category', $category);
	}

	public function update(Request $request, $id)
	{
		$category = Category::find($id);
		$category->name = $request->name;
		$category->save();

		flash('La Categoría:  '.$category->name.' ha sido editada con exito!', 'warning')->important();
		return redirect()->route('categories.index');
	}
}