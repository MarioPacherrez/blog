<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;

use App\Tag;
use Laracasts\Flash\Flash;

class TagsController extends Controller
{
    //Está función es para vista admin.
    public function index(Request $request)
    {
    	$tags = Tag::search($request->name)->orderBy('id','DESC')->paginate(5);
    	return view('admin.tags.index')->with('tags', $tags);
    }

    public function create()
    {
    	return view('admin.tags.create');
    }

    public function store(TagRequest $request)
    {
    	$tag = new Tag($request->all());
    	$tag->save();

    	flash('El tag '.$tag->name.' ha sido creado con exito!', 'success')->important();
		return redirect()->route('tags.index');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
    	$tag = Tag::find($id);
		return view('admin.tags.edit')->with('tag', $tag);
    }

    public function update(Request $request, $id)
    {
    	$tag = Tag::find($id);
		$tag->fill($request->all());
		$tag->save();

		flash('El tag :  '.$tag->name.' ha sido editado con exito!', 'warning')->important();
		return redirect()->route('tags.index');
    }

    public function destroy($id)
    {
    	$tag = Tag::find($id);
		$tag->delete();

		flash('El tag '.$tag->name.' ha sido borrado!', 'warning')->important();
		return redirect()->route('tags.index');
    }
}
