<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Article extends Model{
	protected $table = "articles";

	use Sluggable;
	use SluggableScopeHelpers;

	protected $fillable = ['title','content','category_id','user_id'];

	public function category(){
		return $this->belongsTo('App\Category');
	}

	public function user(){
		return $this->belongsTo('App\User');
	}

	public function images(){
		return $this->hasMany('App\Image');
	}

	public function tags(){
		return $this->belongsToMany('App\Tag');
	}

	public function sluggable(){
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}

	public function scopeSearch($query, $title)
	{
		return $query->where('title', 'LIKE', '%'.$title.'%');
	}
}