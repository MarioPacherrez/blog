<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', ['as' => 'front.index', function () {
//     return view('front.index');
// }]);

//RUTAS DEL FRONTEND
// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [
	'as' => 'front.index',
	'uses' => 'FrontController@index'
	]);

Route::get('categories/{name}', [
	'uses' 	=> 'FrontController@searchCategory',
	'as' 	=> 'front.search.category'
	]);

Route::get('tags/{name}', [
	'uses' 	=> 'FrontController@searchTag',
	'as' 	=> 'front.search.tag'
	]);

Route::get('articles/{slug}', [
	'uses' => 'FrontController@viewArticle',
	'as' => 'front.view.article'
	]);


//RUTAS DEL PANEL DE ADMINISTRACIÓN
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {

	// Route::get('/', ['as' => 'admin.index', function () {
	// 	return view('welcome');
	// }]);

	Route::get('/', function () {
		return view('welcome');
	});

	Route::group(['middleware' => 'admin'], function() {
		
		Route::resource('users', 'UsersController');
		Route::get('users/{id}/destroy', [
			'uses' => 'UsersController@destroy', 
			'as' => 'admin.users.destroy'
			]);
	});

	Route::resource('categories', 'CategoriesController');
	Route::get('categories/{id}/destroy', [
		'uses' => 'CategoriesController@destroy', 
		'as' => 'admin.categories.destroy'
		]);

	Route::resource('tags', 'TagsController');
	Route::get('tags/{id}/destroy', [
		'uses' => 'TagsController@destroy', 
		'as' => 'admin.tags.destroy'
		]);

	Route::resource('articles', 'ArticlesController');
	Route::get('articles/{id}/destroy', [
		'uses' => 'ArticlesController@destroy', 
		'as' => 'admin.articles.destroy'
		]);

	Route::get('images', [
		'uses' => 'ImagesController@index',
		'as'   => 'images.index'
		]);

});
Auth::routes();

Route::get('/home', 'HomeController@index');

// Authentication routes...

// Route::get('admin/auth/login', [
// 	'uses' => 'Auth\LoginController@getLogin', 
// 	'as' => 'admin.auth.login'
// 	]);

// Route::post('admin/auth/login', [
// 	'uses' => 'Auth\LoginController@postLogin', 
// 	'as' => 'admin.auth.login'
// 	]);


// Route::get('admin/auth/logout', [
// 	'uses' => 'Auth\LoginController@getLogout', 
// 	'as' => 'admin.auth.logout'
// 	]);
