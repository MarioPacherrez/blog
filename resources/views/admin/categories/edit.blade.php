@extends('admin.template.main')

@section('title', 'Editar Catgoría: '.$category->name)

@section('content')
	{!! Form::open(['route' => ['categories.update', $category], 'method' => 'PUT']) !!}
	{{-- {!! Form::model($category, array('category' => array('categories.update', $category->id), 'method' => 'PUT')) !!} --}}
		<div class="form-group">
			{!! Form::label('name', 'Nombre') !!}
			{!! Form::text('name', $category->name, ['class' => 'form-control', 'placeholder' => 'Nombre de la Categoría', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
		</div>
	{!! Form::close() !!}
@endsection