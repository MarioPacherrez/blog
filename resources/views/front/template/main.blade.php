<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title', 'Home') | Blog Facilito</title>
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">
</head>
<body>
	<header>
		@include('front.template.partials.header')
	</header>
	<div class="container">
		@yield('content')
		<footer>
			<hr>
			Todos los derechos reservados $copy {{ date('Y') }}
			<div class="pull-right">:v</div>
		</footer>
	</div>

	<script src="{{ asset('plugins/jquery/js/jquery-3.1.1.js') }}"></script>
</body>
</html>